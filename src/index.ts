import * as fs from 'fs';
import path from 'path';

import bodyParser = require('body-parser');
import express from 'express';
import open from 'open';

import { getTranslations } from './translations/get/getTranslations';
import { postTranslations } from './translations/post/postTranslations';

const directory: string = getDirectory();

const app = express();
const PORT = 3000;

app.set('working-directory', directory);

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

// routes
app.get('/api/translations', getTranslations);
app.post('/api/translations', postTranslations);

// serving client
app.use(express.static(path.normalize(`${__dirname}/website`)));

// error handling
app.use((error: any, _req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (res.headersSent) {
        return next(error);
    }
    res.status(500).send({ error: error.message });
});

function getDirectory(): string {
    try {
        if (process.argv.length >= 3) {
            const path: string = process.argv[2];
            if (fs.lstatSync(path).isDirectory()) {
                return path;
            } else {
                throw new Error(`${path} is not directory. Exiting ...`);
            }
        }
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
    return '.';
}

function start(): void {
    app.listen(PORT, () => {
        const url: string = `http://localhost:${PORT}`;
        console.log(`Translau is running on port ${url} working with '${directory}' folder.`);
        open(url);
    });
}

export default start;
