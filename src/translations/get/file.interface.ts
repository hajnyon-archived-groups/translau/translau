import { IData } from './data.interface';

export interface IFile {
	language: string;
	data: IData;
}